import { createApp } from "vue";
import App from "./App.vue";

import "./assets/main.css";

const appEl = document.getElementById("adurmapa");
const adurmapa = createApp(App, {
  ie: appEl?.hasAttribute("ie") && appEl?.getAttribute('ie') !== "false",
});
adurmapa.mount("#adurmapa");
