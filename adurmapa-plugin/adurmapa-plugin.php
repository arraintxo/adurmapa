<?php
/*
Plugin Name: Adurmapa
Description: [adurmapa] plugin-a
Version: 1.0
*/

function enqueue_scripts()
{
    global $post;
    if ($post && has_shortcode($post->post_content, "adurmapa")) {
        $manifestData = json_decode(file_get_contents(__DIR__ . '/dist/manifest.json'), true);
        wp_enqueue_script('adurmapa', plugin_dir_url(__FILE__) . 'dist/' . $manifestData['index.html']['file'], [], null, true);
        wp_enqueue_style('adurmapa', plugin_dir_url(__FILE__) . 'dist/' . $manifestData['index.css']['file']);
    }
}

function handle_shortcode()
{
    $atts = shortcode_atts(
        array(
            'height' => '65vh',
            'width' => '100%',
        ),
        [],
        'adurmapa'
    );
    return '<div id="adurmapa" style="height: ' . $atts['height'] . '; width: ' . $atts['width'] . '"></div>';
}

add_shortcode('adurmapa', 'handle_shortcode');
add_action('wp_enqueue_scripts', 'enqueue_scripts');


